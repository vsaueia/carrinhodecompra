package br.com.improving.carrinho;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class ItemTeste {
	private Produto produto;
	private int quantidadeDoItem = 5;
	private BigDecimal valorUnitarioDoItem = new BigDecimal("23.5");

	@Before
	public void iniciar() {
		produto = new Produto(1L, "Chocolate Laka");
	}

	@Test
	public void deveCriarUmItem() {
		Item item = new Item(produto, valorUnitarioDoItem, quantidadeDoItem);

		assertEquals(item.getProduto(), produto);
		assertEquals(item.getValorUnitario(), valorUnitarioDoItem);
		assertEquals(item.getQuantidade(), quantidadeDoItem);
	}

	@Test(expected = IllegalArgumentException.class)
	public void naoDeveCriarItemSemProduto() {
		new Item(null, valorUnitarioDoItem, quantidadeDoItem);
	}

	@Test(expected = IllegalArgumentException.class)
	public void naoDeveCriarItemSemValorUnitario() {
		new Item(produto, null, quantidadeDoItem);
	}

	@Test(expected = IllegalArgumentException.class)
	public void naoDeveCriarItemComValorUnitarioMenorOuIgualAZero() {
		new Item(produto, BigDecimal.valueOf(-1L), quantidadeDoItem);
	}

	@Test(expected = IllegalArgumentException.class)
	public void naoDeveCriarItemComQuantidadeMenorIgualAZero() {
		new Item(produto, valorUnitarioDoItem, 0);
	}

	@Test
	public void deveObterOValorTotalDoItem() {
		BigDecimal valorTotalEsperado = valorUnitarioDoItem.multiply(BigDecimal.valueOf(quantidadeDoItem));

		Item item = new Item(produto, valorUnitarioDoItem, quantidadeDoItem);

		assertEquals(valorTotalEsperado, item.getValorTotal());
	}
}
