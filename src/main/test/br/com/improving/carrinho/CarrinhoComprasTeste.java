package br.com.improving.carrinho;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

public class CarrinhoComprasTeste {
	private String identificacaoDoCliente;
	private Produto produto;
	private BigDecimal valorUnitarioDoProduto = BigDecimal.valueOf(25L);
	private int quantidadeDoProduto = 4;
	private Long codigoDoProduto = 1L;
	private String descricaoDoProduto = "chocolate laka";
	private CarrinhoCompras carrinhoCompras;

	@Before
	public void iniciar() {
		identificacaoDoCliente = UUID.randomUUID().toString();
		produto = new Produto(codigoDoProduto, descricaoDoProduto);
		carrinhoCompras = new CarrinhoComprasFactory().criar(identificacaoDoCliente);
	}

	@Test
	public void deveAdicionarUmItemNoCarrinhoDeCompras() {
		carrinhoCompras.adicionarItem(produto, valorUnitarioDoProduto, quantidadeDoProduto);

		assertTrue(carrinhoCompras
				   .getItens().stream()
				   .anyMatch(item -> item.getProduto().equals(produto)));
	}

	@Test
	public void deveSomarANovaQuantidadeAQuantidadeDoItemJaExistenteNoCarrinho() {
		int incrementoDeQuantidade = 6;
		int quantidadeEsperadaDoItem = quantidadeDoProduto + incrementoDeQuantidade;
		carrinhoCompras.adicionarItem(produto, valorUnitarioDoProduto, quantidadeDoProduto);

		carrinhoCompras.adicionarItem(produto, valorUnitarioDoProduto, incrementoDeQuantidade);

		assertEquals(quantidadeEsperadaDoItem, carrinhoCompras.getItens().stream()
				.filter(item -> item.getProduto().equals(produto)).findFirst().get().getQuantidade());
	}

	@Test
	public void deveAlterarOValorUnitarioDoItemJaExistenteNoCarrinho() {
		carrinhoCompras.adicionarItem(produto, valorUnitarioDoProduto, quantidadeDoProduto);
		BigDecimal novoValorUnitario = BigDecimal.ONE;

		carrinhoCompras.adicionarItem(produto, novoValorUnitario, 1);

		assertEquals(novoValorUnitario, carrinhoCompras.getItens().stream()
				.filter(item -> item.getProduto().equals(produto)).findFirst().get().getValorUnitario());
	}

	@Test(expected = RuntimeException.class)
	public void deveLancarExcecaoSenaoConseguirAdicionarItem() {
		carrinhoCompras.adicionarItem(null, null, 0);
	}

	@Test
	public void deveRemoverOItemDoCarrinhoDeCompras() {
		carrinhoCompras.adicionarItem(produto, valorUnitarioDoProduto, quantidadeDoProduto);

		boolean itemFoiRemovido = carrinhoCompras.removerItem(produto);

		assertTrue(itemFoiRemovido);
		assertTrue(carrinhoCompras.getItens().stream().noneMatch(item -> item.getProduto().equals(produto)));
	}

	@Test
	public void deveRemoverOItemDoCarrinhoDeComprasPelaPosicao() {
		carrinhoCompras.adicionarItem(produto, valorUnitarioDoProduto, quantidadeDoProduto);

		boolean itemFoiRemovido = carrinhoCompras.removerItem(0);

		assertTrue(itemFoiRemovido);
		assertTrue(carrinhoCompras.getItens().stream().noneMatch(item -> item.getProduto().equals(produto)));
	}

	@Test
	public void naoDeveRemoverItemQueNaoExistaNoCarrinho() {
		carrinhoCompras.adicionarItem(produto, valorUnitarioDoProduto, quantidadeDoProduto);
		Produto outroProduto = new Produto(2L, "outro produto");

		boolean itemFoiRemovido = carrinhoCompras.removerItem(outroProduto);

		assertFalse(itemFoiRemovido);
		assertTrue(carrinhoCompras.getItens().stream().noneMatch(item -> item.getProduto().equals(outroProduto)));
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void naoDeveRemoverItemSePosicaoNaoExisteNoCarrinhoDeCompra() {
		carrinhoCompras.adicionarItem(produto, valorUnitarioDoProduto, quantidadeDoProduto);

		carrinhoCompras.removerItem(1);
	}

	@Test
	public void deveObterOValorTotalDoCarrinhoDeCompras() {
		Produto outroProduto = new Produto(3L, "chocolate milka");
		BigDecimal valorDoOutroProduto = BigDecimal.valueOf(10L);
		int quantidadeDoOutroProduto = 2;
		carrinhoCompras.adicionarItem(produto, valorUnitarioDoProduto, quantidadeDoProduto);
		carrinhoCompras.adicionarItem(outroProduto, valorDoOutroProduto, quantidadeDoOutroProduto);
		BigDecimal valorTotalEsperado = valorUnitarioDoProduto.multiply(BigDecimal.valueOf(quantidadeDoProduto))
				.add(valorDoOutroProduto.multiply(BigDecimal.valueOf(quantidadeDoOutroProduto)));

		BigDecimal valorTotalDoCarrinho = carrinhoCompras.getValorTotal();

		assertEquals(valorTotalEsperado, valorTotalDoCarrinho);
	}

	@Test
	public void deveRetornarUmItemParaAdicaoDeProdutosIguais() {
		carrinhoCompras.adicionarItem(produto, valorUnitarioDoProduto, 2);
		carrinhoCompras.adicionarItem(produto, valorUnitarioDoProduto.add(BigDecimal.ONE), 1);

		assertEquals(1, carrinhoCompras.getItens().size());
	}
}
