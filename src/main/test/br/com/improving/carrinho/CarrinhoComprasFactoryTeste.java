package br.com.improving.carrinho;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

public class CarrinhoComprasFactoryTeste {
	private String identificacaoDoCliente = UUID.randomUUID().toString();
	private CarrinhoComprasFactory carrinhoDeComprasFactory;

	@Before
	public void iniciar() {
		carrinhoDeComprasFactory = new CarrinhoComprasFactory();
	}

	@Test
	public void deveCriarUmNovoCarrinhoDeComprasParaUmNovoCliente() {
		CarrinhoCompras carrinhoDeCompras = carrinhoDeComprasFactory.criar(identificacaoDoCliente);

		assertNotNull(carrinhoDeCompras);
	}

	@Test
	public void deveEncontrarOCarrinhoDoClienteSeJaExistir() {
		carrinhoDeComprasFactory = new CarrinhoComprasFactory();
		CarrinhoCompras carrinhoDeCompras = carrinhoDeComprasFactory.criar(identificacaoDoCliente);

		CarrinhoCompras copiaDoCarrinhoDeCompras = carrinhoDeComprasFactory.criar(identificacaoDoCliente);

		assertSame(carrinhoDeCompras, copiaDoCarrinhoDeCompras);
	}

	@Test
	public void deveCalcularOTicketMedioDosCarrinhosDeCompras() {
		CarrinhoCompras carrinhoDeComprasCliente1 = carrinhoDeComprasFactory.criar(identificacaoDoCliente);
		Produto produtoCliente1 = new Produto(1L, "chocolate");
		BigDecimal valorDoProdutoDoCliente1 = new BigDecimal("10.62");
		int quantidadeDoProdutoDoCliente1 = 1;
		carrinhoDeComprasCliente1.adicionarItem(produtoCliente1, valorDoProdutoDoCliente1, quantidadeDoProdutoDoCliente1);
		String identificacaoDoCliente2 = UUID.randomUUID().toString();
		CarrinhoCompras carrinhoDeComprasCliente2 = carrinhoDeComprasFactory.criar(identificacaoDoCliente2);
		Produto produtoCliente2 = new Produto(2L, "chocolate");
		BigDecimal valorDoProdutoDoCliente2 = new BigDecimal("50.89");
		int quantidadeDoProdutoDoCliente2 = 1;
		carrinhoDeComprasCliente2.adicionarItem(produtoCliente2, valorDoProdutoDoCliente2, quantidadeDoProdutoDoCliente2);
		BigDecimal ticketMedioEsperado = valorDoProdutoDoCliente1
					.multiply(BigDecimal.valueOf(quantidadeDoProdutoDoCliente1))
					.add(valorDoProdutoDoCliente2.multiply(BigDecimal.valueOf(quantidadeDoProdutoDoCliente2)))
					.divide(BigDecimal.valueOf(2), 2, RoundingMode.HALF_UP);

		BigDecimal tickeMedioDosCarrinhosDeCompra = carrinhoDeComprasFactory.getValorTicketMedio();

		assertEquals(ticketMedioEsperado, tickeMedioDosCarrinhosDeCompra);
	}

	@Test
	public void deveCalcularOTicketMedioDosCarrinhosDeComprasComArredomentoParaBaixo() {
		CarrinhoCompras carrinhoDeComprasCliente1 = carrinhoDeComprasFactory.criar(identificacaoDoCliente);
		Produto produtoCliente1 = new Produto(1L, "chocolate");
		BigDecimal valorDoProdutoDoCliente1 = new BigDecimal("10");
		int quantidadeDoProdutoDoCliente1 = 1;
		carrinhoDeComprasCliente1.adicionarItem(produtoCliente1, valorDoProdutoDoCliente1, quantidadeDoProdutoDoCliente1);
		String identificacaoDoCliente2 = UUID.randomUUID().toString();
		CarrinhoCompras carrinhoDeComprasCliente2 = carrinhoDeComprasFactory.criar(identificacaoDoCliente2);
		Produto produtoCliente2 = new Produto(2L, "chocolate");
		BigDecimal valorDoProdutoDoCliente2 = new BigDecimal("50.686");
		int quantidadeDoProdutoDoCliente2 = 1;
		carrinhoDeComprasCliente2.adicionarItem(produtoCliente2, valorDoProdutoDoCliente2, quantidadeDoProdutoDoCliente2);
		BigDecimal ticketMedioEsperado = valorDoProdutoDoCliente1
				.multiply(BigDecimal.valueOf(quantidadeDoProdutoDoCliente1))
				.add(valorDoProdutoDoCliente2.multiply(BigDecimal.valueOf(quantidadeDoProdutoDoCliente2)))
				.divide(BigDecimal.valueOf(2), 2, RoundingMode.HALF_UP);

		BigDecimal tickeMedioDosCarrinhosDeCompra = carrinhoDeComprasFactory.getValorTicketMedio();

		assertEquals(ticketMedioEsperado, tickeMedioDosCarrinhosDeCompra);
	}

	@Test
	public void deveSerPossivelConsultarSeClienteTemCarrinhoDeComprasAtivo() {
		carrinhoDeComprasFactory.criar(identificacaoDoCliente);

		boolean clienteComCarrinhoDeComprasAtivo = carrinhoDeComprasFactory.verificarSeClienteEstaComCarrinhoAtivo(identificacaoDoCliente);

		assertTrue(clienteComCarrinhoDeComprasAtivo);
	}

	@Test
	public void deveRetornarFalsoAoConsultarClienteSemCarrinhoDeComprasAtivo() {
		boolean clienteComCarrinhoDeComprasAtivo = carrinhoDeComprasFactory.verificarSeClienteEstaComCarrinhoAtivo(identificacaoDoCliente);

		assertFalse(clienteComCarrinhoDeComprasAtivo);
	}

	@Test
	public void deveInvalidarOCarrinhoDeComprasDeCliente() {
		carrinhoDeComprasFactory.criar(identificacaoDoCliente);

		boolean carrinhoInvalidado = carrinhoDeComprasFactory.invalidar(identificacaoDoCliente);

		assertTrue(carrinhoInvalidado);
		boolean clienteComCarrinhoDeComprasAtivo = carrinhoDeComprasFactory.verificarSeClienteEstaComCarrinhoAtivo(identificacaoDoCliente);
		assertFalse(clienteComCarrinhoDeComprasAtivo);
	}

	@Test
	public void naoDeveInvalidarOCarrinhoDeComprasDeClienteSeNaoEncontrarOCliente() {
		boolean carrinhoInvalidado = carrinhoDeComprasFactory.invalidar(identificacaoDoCliente);

		assertFalse(carrinhoInvalidado);
	}
}
