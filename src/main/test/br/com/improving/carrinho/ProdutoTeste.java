package br.com.improving.carrinho;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

public class ProdutoTeste {
	private Long codigoDoProduto;
	private String descricaoDoProduto;

	@Before
	public void iniciarCenarios() {
		codigoDoProduto = 1L;
		descricaoDoProduto = "Chocolate Laka";
	}

	@Test
	public void deveCriarUmProduto() {
		Produto produto = new Produto(codigoDoProduto, descricaoDoProduto);

		assertEquals(produto.getCodigo(), codigoDoProduto);
		assertEquals(produto.getDescricao(), descricaoDoProduto);
	}

	@Test(expected = IllegalArgumentException.class)
	public void naoDeveCriarProdutoSemCodigo() {
		new Produto(null, descricaoDoProduto);
	}

	@Test(expected = IllegalArgumentException.class)
	public void naoDeveCriarProdutoSemDescricao() {
		new Produto(codigoDoProduto, null);
	}

	@Test
	public void produtosComMesmosCodigosDevemSerIguais() {
		Produto produto = new Produto(codigoDoProduto, descricaoDoProduto);
		Produto produtoComMesmoCodigo = new Produto(codigoDoProduto, "Chocolate Bis");

		assertEquals(produto, produtoComMesmoCodigo);
	}

	@Test
	public void produtosComCodigosDiferentesNaoDevemSerIguais() {
		Produto produto = new Produto(codigoDoProduto, descricaoDoProduto);
		Produto outroProduto = new Produto(2L, "Chocolate Bis");

		assertNotEquals(produto, outroProduto);
	}

}
