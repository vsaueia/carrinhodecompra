package br.com.improving.carrinho;

import java.math.BigDecimal;

/**
 * Classe que representa um item no carrinho de compras.
 */
public class Item {

    private Produto produto;
    private BigDecimal valorUnitario;
    private int quantidade;

    /**
     * Construtor da classe Item.
     * 
     * @param produto
     * @param valorUnitario
     * @param quantidade
     */
    public Item(Produto produto, BigDecimal valorUnitario, int quantidade) {
		validarProdutoParaOItem(produto);
		validarValorUnitarioParaOItem(valorUnitario);
		validarQuantidadeParaOItem(quantidade);
		this.produto = produto;
		this.valorUnitario = valorUnitario;
		this.quantidade = quantidade;
    }

	/**
     * Retorna o produto.
     *
     * @return Produto
     */
    public Produto getProduto() {
    	return this.produto;
    }

    /**
     * Retorna o valor unitário do item.
     *
     * @return BigDecimal
     */
    public BigDecimal getValorUnitario() {
		return this.valorUnitario;
    }

    /**
     * Retorna a quantidade dos item.
     *
     * @return int
     */
    public int getQuantidade() {
    	return this.quantidade;
    }

    /**
     * Retorna o valor total do item.
     *
     * @return BigDecimal
     */
    public BigDecimal getValorTotal() {
		return calcularValorTotal();
    }

	private BigDecimal calcularValorTotal() {
		return this.valorUnitario.multiply(BigDecimal.valueOf(this.quantidade));
	}

	private void validarQuantidadeParaOItem(int quantidade) throws IllegalArgumentException {
		if (quantidade <= 0) {
			throw new IllegalArgumentException("A quantidade do item deve ser maior que zero");
		}
	}

	private void validarValorUnitarioParaOItem(BigDecimal valorUnitario) {
		if (valorUnitario == null) {
			throw new IllegalArgumentException("O valor unitário do item deve ser informado");
		}
		if (valorUnitario.compareTo(BigDecimal.ZERO) <= 0) {
			throw new IllegalArgumentException("O valor unitário do item deve ser maior que zero");
		}
	}

	private void validarProdutoParaOItem(Produto produto) throws IllegalArgumentException{
		if (produto == null) {
			throw new IllegalArgumentException("O produto do item deve ser informado");
		}
	}

	public void atualizar(BigDecimal valorUnitario, int quantidade) {
		this.valorUnitario = valorUnitario;
		this.quantidade += quantidade;

	}
}
