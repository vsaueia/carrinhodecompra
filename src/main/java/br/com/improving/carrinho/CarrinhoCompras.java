package br.com.improving.carrinho;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

/**
 * Classe que representa o carrinho de compras de um cliente.
 */
public class CarrinhoCompras {

	private ArrayList<Item> itens;

	private CarrinhoCompras() {
		itens = new ArrayList<>();
	}

	static CarrinhoCompras obterNovoCarrinhoDeCompras() {
		return new CarrinhoCompras();
	}
    /**
     * Permite a adição de um novo item no carrinho de compras.
     *
     * Caso o item já exista no carrinho para este mesmo produto, as seguintes regras deverão ser seguidas:
     * - A quantidade do item deverá ser a soma da quantidade atual com a quantidade passada como parâmetro.
     * - Se o valor unitário informado for diferente do valor unitário atual do item, o novo valor unitário do item deverá ser
     * o passado como parâmetro.
     *
     * Devem ser lançadas subclasses de RuntimeException caso não seja possível adicionar o item ao carrinho de compras.
     *
     * @param produto
     * @param valorUnitario
     * @param quantidade
     */
    public void adicionarItem(Produto produto, BigDecimal valorUnitario, int quantidade) throws IllegalArgumentException {
		Optional<Item> itemEncontrado = procurarItemNoCarrinhoDeCompras(produto);
		registrarItem(itemEncontrado, produto, valorUnitario, quantidade);
    }

	/**
     * Permite a remoção do item que representa este produto do carrinho de compras.
     *
     * @param produto
     * @return Retorna um boolean, tendo o valor true caso o produto exista no carrinho de compras e false
     * caso o produto não exista no carrinho.
     */
    public boolean removerItem(Produto produto) {
		Optional<Item> item = procurarItemNoCarrinhoDeCompras(produto);
		return item.isPresent() ? getItens().remove(item.get()): false;
    }

    /**
     * Permite a remoção do item de acordo com a posição.
     * Essa posição deve ser determinada pela ordem de inclusão do produto na 
     * coleção, em que zero representa o primeiro item.
     *
     * @param posicaotItem
     * @return Retorna um boolean, tendo o valor true caso o produto exista no carrinho de compras e false
     * caso o produto não exista no carrinho.
     */
    public boolean removerItem(int posicaotItem) throws IndexOutOfBoundsException {
    	return this.itens.remove(posicaotItem) != null ? true : false;
    }

    /**
     * Retorna o valor total do carrinho de compras, que deve ser a soma dos valores totais
     * de todos os itens que compõem o carrinho.
     *
     * @return BigDecimal
     */
    public BigDecimal getValorTotal() {
		return  this.itens.stream().map(item ->
					item.getValorTotal()).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    /**
     * Retorna a lista de itens do carrinho de compras.
     *
     * @return itens
     */
    public Collection<Item> getItens() {
		return this.itens;
    }

	private void registrarItem(Optional<Item> itemEncontrado, Produto produto, BigDecimal valorUnitario, int quantidade) {
		Item item;
		if (!itemEncontrado.isPresent()) {
			item = new Item(produto, valorUnitario, quantidade);
			itens.add(item);
		} else {
			item = itemEncontrado.get();
			item.atualizar(valorUnitario, quantidade);
		}
	}


	private Optional<Item> procurarItemNoCarrinhoDeCompras(Produto produto) throws IllegalArgumentException {
		Optional<Item> itemBuscado = this.getItens().stream().filter(item -> item.getProduto().equals(produto)).findFirst();
		return itemBuscado;
	}
}